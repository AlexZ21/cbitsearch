#include "cbitsearch.h"

#include <iostream>
#include <algorithm>

#include "../../udutils.h"

#include <QDebug>
#include <QString>

CBitSearch::CBitSearch()
{
}

CBitSearch::CBitSearch(const std::string &mask) :
    _reverseBits(false), _maskBitCount(0)
{
    setMask(mask);
}

std::string CBitSearch::mask() const
{
    return _mask;
}

void CBitSearch::setMask(const std::string &mask)
{
    _mask = mask;

    _maskVec.clear();

    int byteCount = mask.size()/8;
    int bitCount = mask.size()%8;
    if (bitCount > 0)
        ++byteCount;

    _maskVec.resize(byteCount);
    _maskBitCount = 0;

    for (int i = 0; i < byteCount; ++i) {
        _maskVec[i].byte = 0;
        _maskVec[i].bitCount = (i == byteCount - 1) ? bitCount : 8;
        if (_maskVec[i].bitCount == 0) _maskVec[i].bitCount = 8;
        _maskBitCount += _maskVec[i].bitCount;

        for (int j = 0; j < 8; ++j) {
            if (i * 8 + j < mask.size()) {
                char b = mask[i * 8 + j];
                bool bit = b == '1' ? true : false;
                if (!_reverseBits)
                    _maskVec[i].byte ^= (-bit ^ _maskVec[i].byte) & (1 << 7 - j);
                else
                    _maskVec[i].byte ^= (-bit ^ _maskVec[i].byte) & (1 << j);
            }
        }
    }

    _tmpVec.clear();
    _tmpVec.resize(_maskVec.size());
    for (int i = 0; i < _tmpVec.size(); ++i) {
        _tmpVec[i].byte = 0;
        _tmpVec[i].bitCount = _maskVec[i].bitCount;
    }

}

int CBitSearch::find(const char *data, int len, int offset)
{
    if (_mask.empty())
        return -1;

    int bit = findHelper(data, len, offset);
    if (!_reverseBits) {
        if (bit != -1){
            int tmpByte = (bit - _maskBitCount)/8;
            int tmpBit = (bit - _maskBitCount)%8;
            int startBit = -1;
            if (8 - (tmpBit + _maskBitCount) > 0)
                startBit = 8 - (tmpBit + _maskBitCount) + tmpByte*8;
            else
                startBit = tmpByte*8;
            return startBit;
        }
    } else {
        return bit - _maskVec.size()*8;
    }
    return bit;
}

bool CBitSearch::isMask(int pos, const char *data, int len)
{
    for (size_t i = 0; i < _tmpVec.size(); ++i) {
        _tmpVec[i].byte = 0;
        _tmpVec[i].bitCount = _maskVec[i].bitCount;
    }

    if (len < _tmpVec.size() + 1)
        return false;

    int posByte = pos/8;
    int posBit = pos%8;

    if (posBit == 0) {
        for (size_t i = 0; i < _tmpVec.size(); ++i)
            _tmpVec[i].byte = data[posByte + i];
        return matchMaskHelper(_tmpVec);
    } else {
        for (size_t i = 0; i < _tmpVec.size(); ++i) {
            _tmpVec[i].byte = data[posByte + i];

            if (!_reverseBits)
                _tmpVec[i].byte <<= posBit;
            else
                _tmpVec[i].byte >>= posBit;

            char c = data[posByte + i + 1];

            char mask = (!_reverseBits) ? 255 >> (8 - posBit) : 255 << (8 - posBit);
            char mask2 = (!_reverseBits) ? 255 << posBit : 255 >> posBit;

            _tmpVec[i].byte = _tmpVec[i].byte & mask2;
            _tmpVec[i].byte |= (!_reverseBits) ? (c >> (8 - posBit)) & mask : (c << (8 - posBit)) & mask;
        }

        return matchMaskHelper(_tmpVec);
    }

    return false;
}

bool CBitSearch::reverseBits() const
{
    return _reverseBits;
}

void CBitSearch::setReverseBits(bool reverseBits)
{
    _reverseBits = reverseBits;
    setMask(_mask);
}

int CBitSearch::maskBitCount() const
{
    return _maskBitCount;
}

int CBitSearch::findHelper(const char *data, int len, int offset)
{
    for (int i = 0; i < _tmpVec.size(); ++i) {
        _tmpVec[i].byte = 0;
        _tmpVec[i].bitCount = _maskVec[i].bitCount;
    }

    if (len < _tmpVec.size())
        return -1;

    int bitLen = len * 8 + _tmpVec.size()*8;
    for (int i = offset; i < bitLen; ++i) {
        if (matchMaskHelper(_tmpVec))
            return i;

        if ((i + _maskBitCount == bitLen))
            break;

        for (int j = 0; j < _tmpVec.size(); ++j) {
            if (!_reverseBits)
                _tmpVec[j].byte <<= 1;
            else
                _tmpVec[j].byte >>= 1;

            if (j == _tmpVec.size() - 1) {
                int tmpByte = i/8;
                int tmpBit = i%8;
                int nextBit = _reverseBits ? tmpBit : 7 - tmpBit;
                int nextBit2 = _reverseBits ? 7 : 0;
                _tmpVec[j].byte ^= (-((data[tmpByte] >> nextBit) & 1) ^ _tmpVec[j].byte) & (1 << nextBit2);
            } else {
                int nextBit = _reverseBits ? 0 : 7;
                int nextBit2 = _reverseBits ? 7 : 0;
                _tmpVec[j].byte ^= (-((_tmpVec[j + 1].byte >> nextBit) & 1) ^ _tmpVec[j].byte) & (1 << nextBit2);
            }
        }

    }

    return -1;
}

bool CBitSearch::matchMaskHelper(const std::vector<CBitSearch::Byte> &vec)
{
    bool matched = true;
    for (int j = 0; j < vec.size(); ++j) {
        char mask = (!_reverseBits) ? 255 << (8 - _maskVec[j].bitCount) : 255 >> (8 - _maskVec[j].bitCount);
        if ((vec[j].byte & mask) != _maskVec[j].byte) {
            matched = false;
            break;
        }
    }
    return matched;
}
