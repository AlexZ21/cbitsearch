#ifndef CBITSEARCH_H
#define CBITSEARCH_H

#include <string>
#include <vector>

class CBitSearch
{
public:
    struct Byte {
        Byte() : byte(0), bitCount(0) {}
        char byte;
        int bitCount;
    };

    CBitSearch();
    CBitSearch(const std::string &mask);

    std::string mask() const;
    void setMask(const std::string &mask);

    int find(const char *data, int len, int offset = 0);
    bool isMask(int pos, const char *data, int len);

    bool reverseBits() const;
    void setReverseBits(bool reverseBits);

    int maskBitCount() const;

private:
    int findHelper(const char *data, int len, int offset = 0);
    bool matchMaskHelper(const std::vector<Byte> &vec);

private:
    std::string _mask; ///! Маска
    bool _reverseBits; ///! Разворачивать последовательность битов в маске

    std::vector<Byte> _maskVec; ///! Вектор маски
    int _maskBitCount; ///! Длина маски в битах

    std::vector<Byte> _tmpVec; ///! Вектор временных данных
};

#endif // CBITSEARCH_H
