#include <iostream>
#include <string>
#include <cstring>

#include "cbitsearch.h"
#include "cbitcutter.h"

std::string bitsToString(char *s, const int len, bool reverse = true) {
    std::string bitsOutput;
    for (int i = 0; i < len; ++i) {
        char c = s[i];
        if (reverse)
            for (int j = 7; j >= 0; --j)
                bitsOutput.append(((c >> j) & 1) ? "1" : "0");
        else
            for (int j = 0; j < 8; ++j)
                bitsOutput.append(((c >> j) & 1) ? "1" : "0");

        bitsOutput.append(" ");
    }
    return bitsOutput;
}

std::string bitsToString2(char *s, const int len, bool reverse = true) {
    std::string bitsOutput;
    for (int i = len - 1; i >= 0; --i) {
        char c = s[i];
        if (reverse)
            for (int j = 7; j >= 0; --j)
                bitsOutput.append(((c >> j) & 1) ? "1" : "0");
        else
            for (int j = 0; j < 8; ++j)
                bitsOutput.append(((c >> j) & 1) ? "1" : "0");

        bitsOutput.append(" ");
    }
    return bitsOutput;
}

int main(int argc, char *argv[])
{
    char *data = "1z34567890abcdefg";
    int len = strlen(data);

    CBitSearch bitSearch("1100110011011100");
    bitSearch.setReverseBits(true);

    std::cout << "Bit count:        " << (len*8) << std::endl;
    std::cout << "Bits in memory:   " << bitsToString(data, len) << std::endl;
    std::cout << "Reverse bits:     " << bitsToString(data, len, false) << std::endl;

    int pos = bitSearch.find(data, len);

    if (pos != -1)
        std::cout << "Found at:         " <<  pos << std::endl;
    else
        std::cout << "Not found" << std::endl;

    bitSearch.findAndPrint(data, len);

    CBitCutter bitCutter;
    bitCutter.cut(pos, data, len);

    std::cout << "Bits in memory:   " << bitsToString(bitCutter.data(), bitCutter.len()) << std::endl;
    std::cout << "Reverse bits:     " << bitsToString(bitCutter.data(), bitCutter.len(), false) << std::endl;

    return 0;
}
